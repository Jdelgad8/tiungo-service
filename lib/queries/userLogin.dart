String userLoginsQuery = """
  {
    userLogins{
      id
      user_login_email
      user_login_phone_number
    }
  }
""";
