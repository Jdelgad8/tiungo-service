import 'package:flutter/material.dart';

final _icons = <String, IconData>{
  'text': Icons.text_fields,
};

Icon getIcon(String iconName) {
  return Icon(
    _icons[iconName],
    color: Color.fromRGBO(250, 175, 24, 1.0),
  );
}
