import 'package:flutter/material.dart';
import 'package:tiungo_service_client/src/pages/add_service_page.dart';
import 'package:tiungo_service_client/src/pages/profile_page.dart';

class CategoriesPage extends StatefulWidget {
  @override
  _CategoriesPageState createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage> {
  int currentIndex = 2;

  Widget _categoriesPage() {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Theme.of(context).primaryColor,
        title: Text(
          'Categorías',
          style: TextStyle(fontSize: 20.0),
        ),
        actions: <Widget>[
          PopupMenuButton(
            itemBuilder: (BuildContext context) => <PopupMenuEntry>[
              // const PopupMenuItem(
              //   value: 'configuration',
              //   child: Text('Configuración'),
              // ),
              const PopupMenuItem(
                value: 'logout',
                child: Text('Cerrar sesión'),
              ),
            ],
            onSelected: (value) {
              if (value == 'logout') {
                Navigator.pushReplacementNamed(context, 'auth');
              }
            },
          ),
        ],
      ),
      body: GridView.count(
        primary: false,
        padding: const EdgeInsets.all(20),
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        crossAxisCount: 2,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.all(8),
            child: Column(
              children: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.pushNamed(context, 'home_categories');
                  },
                  child: Image.asset(
                    'assets/hogar_category.jpg',
                    width: 130.0,
                    height: 130.0,
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  'Hogar',
                  style: TextStyle(fontSize: 20.0),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.all(8),
            child: Column(
              children: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.pushNamed(context, 'auth');
                  },
                  child: Image.asset(
                    'assets/belleza_category.png',
                    width: 130.0,
                    height: 130.0,
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  'Belleza',
                  style: TextStyle(fontSize: 20.0),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.all(8),
            child: Column(
              children: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.pushNamed(context, 'auth');
                  },
                  child: Image.asset(
                    'assets/ingenieria_category.jpg',
                    width: 130.0,
                    height: 130.0,
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  'Ingeniería',
                  style: TextStyle(fontSize: 20.0),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.all(8),
            child: Column(
              children: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.pushNamed(context, 'auth');
                  },
                  child: Image.asset(
                    'assets/mecanica_category.jpg',
                    width: 130.0,
                    height: 130.0,
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  'Mecánica',
                  style: TextStyle(fontSize: 20.0),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.all(8),
            child: Column(
              children: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.pushNamed(context, 'auth');
                  },
                  child: Image.asset(
                    'assets/logistica_category.png',
                    width: 130.0,
                    height: 130.0,
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  'Logística',
                  style: TextStyle(fontSize: 20.0),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.all(8),
            child: Column(
              children: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.pushNamed(context, 'auth');
                  },
                  child: Image.asset(
                    'assets/salud_category.png',
                    width: 130.0,
                    height: 130.0,
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  'Hogar',
                  style: TextStyle(fontSize: 20.0),
                ),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: new BottomNavigationBar(
        // backgroundColor: Theme.of(context).primaryColor,
        // backgroundColor: Colors.white70,
        backgroundColor: Colors.white,
        currentIndex: currentIndex,
        onTap: (index) {
          setState(() {
            currentIndex = index;
          });
        },
        type: BottomNavigationBarType.fixed,
        // fixedColor: Theme.of(context).primaryColor,
        items: [
          new BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
              color: Colors.black,
            ),
            title: new Text(
              'Perfíl',
              style: TextStyle(color: Colors.grey),
            ),
          ),
          new BottomNavigationBarItem(
            icon: Icon(
              Icons.add_box,
              color: Colors.black,
            ),
            title: new Text('Agregar servicio'),
          ),
          new BottomNavigationBarItem(
            icon: Icon(
              Icons.category,
              color: Colors.black,
            ),
            title: new Text('Categorías'),
          ),
        ],
      ),
    );
  }

  Widget _bottomNavigationSelect(int currentPage) {
    switch (currentPage) {
      case 0:
        return ProfilePage();

      case 1:
        return AddServicePage();
      case 2:
        return _categoriesPage();
      default:
        return ProfilePage();
    }
  }

  @override
  Widget build(BuildContext context) {
    return _bottomNavigationSelect(currentIndex);
  }
}
