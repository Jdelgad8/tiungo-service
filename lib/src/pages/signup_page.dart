import 'package:flutter/material.dart';

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Map<String, dynamic> _formData = {
    'email': null,
    'cellphone': null,
    'password': null,
    'accept_terms': false,
  };
  final TextEditingController _emailTextController = TextEditingController();
  final TextEditingController _cellphoneTextController =
      TextEditingController();
  final TextEditingController _passwordTextController = TextEditingController();

  Widget _buildEmailTextField() {
    return TextFormField(
      onSaved: (String value) {
        _formData['email'] = value;
      },
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.email, color: Theme.of(context).primaryColorLight),
        labelText: 'Correo electrónico',
        labelStyle: TextStyle(color: Theme.of(context).accentColor),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).accentColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).focusColor),
        ),
        filled: true,
        fillColor: Colors.white,
      ),
      controller: _emailTextController,
      validator: (String value) {
        if (value.isEmpty ||
            !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                .hasMatch(value)) {
          return 'Por favor ingresa un correo válido';
        }
        return null;
      },
    );
  }

  Widget _buildEmailConfirmTextField() {
    return TextFormField(
      onSaved: (String value) {
        _formData['email'] = value;
      },
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        focusColor: Theme.of(context).primaryColorLight,
        icon: Icon(Icons.email, color: Theme.of(context).primaryColorLight),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).accentColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).focusColor),
        ),
        labelText: 'Confirmar correo electrónico',
        labelStyle: TextStyle(color: Theme.of(context).accentColor),
        filled: true,
        fillColor: Colors.white,
      ),
      validator: (String value) {
        if (_emailTextController.text != value) {
          return 'Los correos electrónicos no coinciden';
        }
        return null;
      },
    );
  }

  Widget _buildCellphoneTextField() {
    return TextFormField(
      onSaved: (String value) {
        _formData['cellphone'] = value;
      },
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(
        icon: Icon(Icons.phone_android,
            color: Theme.of(context).primaryColorLight),
        labelText: 'Celular',
        labelStyle: TextStyle(color: Theme.of(context).accentColor),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).accentColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).focusColor),
        ),
        filled: true,
        fillColor: Colors.white,
      ),
      controller: _cellphoneTextController,
      validator: (String value) {
        if (value.isEmpty || value.length < 10) {
          return 'Por favor ingresa un número de celular válido';
        }
        return null;
      },
    );
  }

  Widget _buildPasswordTextField() {
    return TextFormField(
      onSaved: (String value) {
        _formData['password'] = value;
      },
      obscureText: true,
      decoration: InputDecoration(
        focusColor: Theme.of(context).primaryColorLight,
        icon: Icon(Icons.lock_outline,
            color: Theme.of(context).primaryColorLight),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          // borderSide: BorderSide(color: Color.fromRGBO(136, 194, 64, 1.0)),
          borderSide: BorderSide(color: Theme.of(context).accentColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).focusColor),
        ),
        labelText: 'Contraseña',
        labelStyle: TextStyle(color: Theme.of(context).accentColor),
        filled: true,
        fillColor: Colors.white,
      ),
      controller: _passwordTextController,
      validator: (String value) {
        if (value.isEmpty || value.length < 6) {
          return 'Por favor ingrese una contraseña al menos de 6 caracteres';
        }
        return null;
      },
    );
  }

  Widget _buildPasswordConfirmTextField() {
    return TextFormField(
      onSaved: (String value) {
        _formData['password'] = value;
      },
      obscureText: true,
      decoration: InputDecoration(
        focusColor: Theme.of(context).primaryColorLight,
        icon: Icon(Icons.lock_outline,
            color: Theme.of(context).primaryColorLight),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).accentColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).focusColor),
        ),
        labelText: 'Confirmar contraseña',
        labelStyle: TextStyle(color: Theme.of(context).accentColor),
        filled: true,
        fillColor: Colors.white,
      ),
      validator: (String value) {
        if (_passwordTextController.text != value) {
          return 'Passwords don\'t match';
        }
        return null;
      },
    );
  }

  Widget _buildAcceptSwitch() {
    return SwitchListTile(
      title: Text('Aceptar términos'),
      value: _formData['accept_terms'],
      onChanged: (bool value) {
        setState(() {
          _formData['accept_terms'] = value;
        });
      },
    );
  }

  _showWarningDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Acepta los términos y condiciones'),
          content:
              Text('Por favor lee y acepta nuestros términos y condiciones.'),
          actions: <Widget>[
            FlatButton(
              child: Text('Regresar'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildSignupButton() {
    return ButtonTheme(
      minWidth: 500.0,
      height: 50.0,
      buttonColor: Theme.of(context).buttonColor,
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(5.0),
      ),
      child: RaisedButton(
        onPressed: () {
          _submitForm();
        },
        child: Text(
          'Continuar',
          style: TextStyle(fontSize: 20.0),
        ),
        textColor: Colors.white,
      ),
    );
  }

  void _submitForm() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    if (!_formData['accept_terms']) {
      setState(() {
        _showWarningDialog(context);
      });
      return;
    }
    Navigator.pushNamed(context, 'signup_personal_data');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text('Registrate'),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(15.0),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 20.0,
                ),
                _buildEmailTextField(),
                SizedBox(height: 10.0),
                _buildEmailConfirmTextField(),
                SizedBox(height: 10.0),
                _buildCellphoneTextField(),
                SizedBox(height: 10.0),
                _buildPasswordTextField(),
                SizedBox(height: 10.0),
                _buildPasswordConfirmTextField(),
                SizedBox(height: 10.0),
                _buildAcceptSwitch(),
                SizedBox(height: 10.0),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 50.0),
                  child: _buildSignupButton(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
