import 'package:flutter/material.dart';

class PlomeriaUsersPage extends StatefulWidget {
  @override
  _PlomeriaUsersPageState createState() => _PlomeriaUsersPageState();
}

class _PlomeriaUsersPageState extends State<PlomeriaUsersPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Plomería'),
        centerTitle: true,
      ),
      body: ListView(
        children: <Widget>[
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  radius: 40.0,
                  backgroundColor: Colors.white,
                  backgroundImage: NetworkImage(
                      'https://scontent.fbog10-1.fna.fbcdn.net/v/t1.0-9/36803760_10156299460118280_6286547981957070848_n.jpg?_nc_cat=104&_nc_oc=AQl4GB7Vpxzij2s5foB8IIhkWSC5ZVmlYVEv-LQpJEikwgj4TJ0cZAGYq5SsyRZq8Fc&_nc_ht=scontent.fbog10-1.fna&oh=d31bc9bfddcd487f8aaf177094642986&oe=5E419C6E'),
                ),
              ),
              Column(
                children: <Widget>[
                  Text(
                    'Jordy Delgado',
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40.0),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star_half,
                          color: Colors.yellow,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: 60.0,
              ),
              IconButton(
                onPressed: () {
                  Navigator.pushNamed(context, 'profile');
                },
                icon: Icon(Icons.arrow_forward_ios, size: 40.0),
              )
            ],
          ),
          Divider(),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  radius: 40.0,
                  backgroundColor: Colors.white,
                  backgroundImage: NetworkImage(
                      'http://iconshow.me/media/images/Mixed/small-n-flat-icon/png/512/user-alt.png'),
                ),
              ),
              Column(
                children: <Widget>[
                  Text(
                    'Adrian Ramos',
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40.0),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: 60.0,
              ),
              Icon(
                Icons.arrow_forward_ios,
                size: 40.0,
              )
            ],
          ),
          Divider(),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  radius: 40.0,
                  backgroundColor: Colors.white,
                  backgroundImage: NetworkImage(
                      'http://iconshow.me/media/images/Mixed/small-n-flat-icon/png/512/user-alt.png'),
                ),
              ),
              Column(
                children: <Widget>[
                  Text(
                    'Adrian Ramos',
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40.0),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: 60.0,
              ),
              Icon(
                Icons.arrow_forward_ios,
                size: 40.0,
              )
            ],
          ),
          Divider(),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  radius: 40.0,
                  backgroundColor: Colors.white,
                  backgroundImage: NetworkImage(
                      'http://iconshow.me/media/images/Mixed/small-n-flat-icon/png/512/user-alt.png'),
                ),
              ),
              Column(
                children: <Widget>[
                  Text(
                    'Adrian Ramos',
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40.0),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: 60.0,
              ),
              Icon(
                Icons.arrow_forward_ios,
                size: 40.0,
              )
            ],
          ),
          Divider(),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  radius: 40.0,
                  backgroundColor: Colors.white,
                  backgroundImage: NetworkImage(
                      'http://iconshow.me/media/images/Mixed/small-n-flat-icon/png/512/user-alt.png'),
                ),
              ),
              Column(
                children: <Widget>[
                  Text(
                    'Adrian Ramos',
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40.0),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: 60.0,
              ),
              Icon(
                Icons.arrow_forward_ios,
                size: 40.0,
              )
            ],
          ),
          Divider(),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  radius: 40.0,
                  backgroundColor: Colors.white,
                  backgroundImage: NetworkImage(
                      'http://iconshow.me/media/images/Mixed/small-n-flat-icon/png/512/user-alt.png'),
                ),
              ),
              Column(
                children: <Widget>[
                  Text(
                    'Adrian Ramos',
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40.0),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: 60.0,
              ),
              Icon(
                Icons.arrow_forward_ios,
                size: 40.0,
              )
            ],
          ),
          Divider(),
        ],
      ),
    );
  }
}
