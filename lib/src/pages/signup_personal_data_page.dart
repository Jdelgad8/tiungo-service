// import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';

class SignupPersonalDataPage extends StatefulWidget {
  @override
  _SignupPersonalDataPageState createState() => _SignupPersonalDataPageState();
}

class _SignupPersonalDataPageState extends State<SignupPersonalDataPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Map<String, dynamic> _formData = {
    'name': null,
    'middle_name': null,
    'last_name': null,
    'birthday': null,
  };
  final TextEditingController _nameTextController = TextEditingController();
  final TextEditingController _middleNameTextController =
      TextEditingController();
  final TextEditingController _lastNameTextController = TextEditingController();
  final TextEditingController _birthdayTextController = TextEditingController();

  Widget _buildNameTextField() {
    return TextFormField(
      onSaved: (String value) {
        _formData['name'] = value;
      },
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.person, color: Theme.of(context).primaryColorLight),
        labelText: 'Primer nombre *',
        labelStyle: TextStyle(color: Theme.of(context).accentColor),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).accentColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).focusColor),
        ),
        filled: true,
        fillColor: Colors.white,
      ),
      controller: _nameTextController,
      validator: (String value) {
        if (value.isEmpty) {
          return 'Por favor ingresa tu nombre';
        }
        return null;
      },
    );
  }

  Widget _buildMiddleNameTextField() {
    return TextFormField(
      onSaved: (String value) {
        _formData['middle_name'] = value;
      },
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        focusColor: Theme.of(context).primaryColorLight,
        icon: Icon(Icons.person, color: Theme.of(context).primaryColorLight),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).accentColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).focusColor),
        ),
        labelText: 'Segundo nombre',
        labelStyle: TextStyle(color: Theme.of(context).accentColor),
        filled: true,
        fillColor: Colors.white,
      ),
      controller: _middleNameTextController,
    );
  }

  Widget _buildLastNameTextField() {
    return TextFormField(
      onSaved: (String value) {
        _formData['last_name'] = value;
      },
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.person, color: Theme.of(context).primaryColorLight),
        labelText: 'Apellidos *',
        labelStyle: TextStyle(color: Theme.of(context).accentColor),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).accentColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).focusColor),
        ),
        filled: true,
        fillColor: Colors.white,
      ),
      controller: _lastNameTextController,
      validator: (String value) {
        if (value.isEmpty) {
          return 'Por favor ingresa tus apellidos';
        }
        return null;
      },
    );
  }

  Widget _buildBirthdayTextField(BuildContext context) {
    return TextFormField(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate(context);
      },
      enableInteractiveSelection: false,
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(
        icon: Icon(Icons.calendar_today,
            color: Theme.of(context).primaryColorLight),
        labelText: 'Fecha de nacimiento *',
        labelStyle: TextStyle(color: Theme.of(context).accentColor),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).accentColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).focusColor),
        ),
        filled: true,
        fillColor: Colors.white,
      ),
      controller: _birthdayTextController,
      validator: (String value) {
        if (value.isEmpty) {
          return 'Por favor selecciona tu fecha de nacimiento';
        }
        return null;
      },
    );
  }

  Future _selectDate(BuildContext context) async {
    DateTime birthdayPicked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(1900),
        lastDate: new DateTime(2025),
        locale: Locale('es', 'ES'));
    if (birthdayPicked != null) {
      setState(() {
        _formData['birthday'] = birthdayPicked.toString().substring(0, 10);
        _birthdayTextController.text = _formData['birthday'];
      });
    }
  }

  Widget _buildSignupButton() {
    return ButtonTheme(
      minWidth: 500.0,
      height: 50.0,
      buttonColor: Theme.of(context).buttonColor,
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(5.0),
      ),
      child: RaisedButton(
        onPressed: () {
          _submitForm();
        },
        child: Text(
          'Continuar',
          style: TextStyle(fontSize: 20.0),
        ),
        textColor: Colors.white,
      ),
    );
  }

  void _submitForm() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    Navigator.pushReplacementNamed(context, 'profile');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text('Datos personales'),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(15.0),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 20.0,
                ),
                _buildNameTextField(),
                SizedBox(height: 10.0),
                _buildMiddleNameTextField(),
                SizedBox(height: 10.0),
                _buildLastNameTextField(),
                SizedBox(height: 10.0),
                _buildBirthdayTextField(context),
                SizedBox(height: 10.0),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 50.0),
                  child: _buildSignupButton(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
