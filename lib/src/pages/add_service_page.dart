import 'package:flutter/material.dart';
import 'package:tiungo_service_client/src/pages/categories_page.dart';
import 'package:tiungo_service_client/src/pages/profile_page.dart';

class AddServicePage extends StatefulWidget {
  @override
  _AddServicePageState createState() => _AddServicePageState();
}

class _AddServicePageState extends State<AddServicePage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Map<String, dynamic> _formData = {
    'service_type': null,
    'service_tittle': null,
    'service_cost': 0,
    'service_tags': null,
    'service_time_cost_unit': null,
    'service_details': null,
    'service_comments': null,
  };
  final TextEditingController _serviceTittleTextController =
      TextEditingController();
  final TextEditingController _serviceCostTextController =
      TextEditingController();
  // final TextEditingController _serviceTagsTextController =
  //     TextEditingController();
  final TextEditingController _serviceDetailsTextController =
      TextEditingController();
  final TextEditingController _serviceCommentsTextController =
      TextEditingController();

  List<String> _serviceCategoryList = [
    'Hogar',
    'Vehículos',
    'Asesoría',
    'Ingeniería',
  ];

  List<String> _serviceTypeList = [
    'Plomería',
    'Mecánica',
    'Carpintería',
    'Diseño gráfico',
  ];

  int currentIndex = 1;

  List<String> _serviceTimeCostUnitList = [
    'Hora',
    'Día',
    'Contrato',
    'Servicio completo',
  ];

  String _selectedServiceCategory;
  String _selectedServiceType;
  String _selectedServiceTimeCostUnit;

  List<DropdownMenuItem<String>> serviceCategoryListDropdownOptions() {
    List<DropdownMenuItem<String>> serviceCategoryList = new List();
    _serviceCategoryList.forEach((serviceCategory) {
      serviceCategoryList.add(DropdownMenuItem(
        child: Text(serviceCategory),
        value: serviceCategory,
      ));
    });
    return serviceCategoryList;
  }

  Widget _buildServiceCategoryDropdown() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50.0),
      child: ButtonTheme(
        minWidth: 500.0,
        alignedDropdown: true,
        child: Container(
          width: 500.0,
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            border: Border.all(
              color: Theme.of(context).accentColor,
              style: BorderStyle.solid,
              // width: 0.80,
            ),
          ),
          child: Row(
            children: <Widget>[
              Expanded(
                child: DropdownButtonHideUnderline(
                  child: DropdownButton(
                    icon: Icon(Icons.keyboard_arrow_down),
                    iconSize: 30,
                    iconEnabledColor: Theme.of(context).accentColor,
                    items: serviceCategoryListDropdownOptions(),
                    value: _selectedServiceCategory,
                    onChanged: (value) {
                      setState(() {
                        _selectedServiceCategory = value;
                      });
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<DropdownMenuItem<String>> serviceTypeListDropdownOptions() {
    List<DropdownMenuItem<String>> serviceTypeList = new List();
    _serviceTypeList.forEach((serviceType) {
      serviceTypeList.add(DropdownMenuItem(
        child: Text(serviceType),
        value: serviceType,
      ));
    });
    return serviceTypeList;
  }

  Widget _buildServiceTypeDropdown() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50.0),
      child: ButtonTheme(
        minWidth: 500.0,
        alignedDropdown: true,
        child: Container(
          width: 500.0,
          height: 50.0,
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            border: Border.all(
              color: Theme.of(context).accentColor,
              style: BorderStyle.solid,
              // width: 0.80,
            ),
          ),
          child: Row(
            children: <Widget>[
              Expanded(
                child: DropdownButtonHideUnderline(
                  child: DropdownButton(
                    icon: Icon(Icons.keyboard_arrow_down),
                    iconSize: 30,
                    iconEnabledColor: Theme.of(context).accentColor,
                    items: serviceTypeListDropdownOptions(),
                    value: _selectedServiceType,
                    onChanged: (value) {
                      setState(() {
                        _selectedServiceType = value;
                      });
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildServiceTittleTextField() {
    return Padding(
      padding: const EdgeInsets.only(right: 50.0, left: 10.0),
      child: TextFormField(
        onSaved: (String value) {
          _formData['service_tittle'] = value;
        },
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          icon: Icon(Icons.title, color: Theme.of(context).primaryColorLight),
          labelText: 'Título del servicio *',
          labelStyle: TextStyle(color: Theme.of(context).accentColor),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide(color: Theme.of(context).accentColor),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide(color: Theme.of(context).focusColor),
          ),
          filled: true,
          fillColor: Colors.white,
        ),
        controller: _serviceTittleTextController,
        validator: (String value) {
          if (value.isEmpty) {
            return 'Por favor ingresa el título del servicio';
          }
          return null;
        },
      ),
    );
  }

  Widget _buildServiceCostTextField() {
    return Padding(
      padding: const EdgeInsets.only(right: 50.0, left: 10.0),
      child: TextFormField(
        onSaved: (String value) {
          _formData['service_cost'] = value;
        },
        keyboardType: TextInputType.numberWithOptions(decimal: true),
        decoration: InputDecoration(
          icon: Icon(
            Icons.attach_money,
            color: Theme.of(context).primaryColorLight,
          ),
          labelText: 'Costo del servicio *',
          labelStyle: TextStyle(color: Theme.of(context).accentColor),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide(color: Theme.of(context).accentColor),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide(color: Theme.of(context).focusColor),
          ),
          filled: true,
          fillColor: Colors.white,
        ),
        controller: _serviceCostTextController,
        validator: (String value) {
          if (value.isEmpty) {
            return 'Por favor ingresa el costo del servicio';
          } else if (int.parse(value) < 0) {
            return 'Por favor ingresa un costo válido';
          }
          return null;
        },
      ),
    );
  }

  Widget _buildServiceTagTextField() {
    return Padding(
      padding: const EdgeInsets.only(right: 50.0, left: 10.0),
      child: TextFormField(
        onSaved: (String value) {
          _formData['service_tags'] = value;
        },
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          icon: Icon(Icons.local_offer,
              color: Theme.of(context).primaryColorLight),
          labelText: 'Etiquetas',
          labelStyle: TextStyle(color: Theme.of(context).accentColor),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide(color: Theme.of(context).accentColor),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide(color: Theme.of(context).focusColor),
          ),
          filled: true,
          fillColor: Colors.white,
        ),
        controller: _serviceTittleTextController,
      ),
    );
  }

  List<DropdownMenuItem<String>> serviceTimeCostUnitListDropdownOptions() {
    List<DropdownMenuItem<String>> serviceTimeCostUnitList = new List();
    _serviceTimeCostUnitList.forEach((serviceTimeCostUnit) {
      serviceTimeCostUnitList.add(DropdownMenuItem(
        child: Text(serviceTimeCostUnit),
        value: serviceTimeCostUnit,
      ));
    });
    return serviceTimeCostUnitList;
  }

  Widget _buildServiceTimeCostUnitDropdown() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50.0),
      child: ButtonTheme(
        minWidth: 260.0,
        alignedDropdown: true,
        child: Container(
          width: 500.0,
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            border: Border.all(
              color: Theme.of(context).accentColor,
              style: BorderStyle.solid,
              // width: 0.80,
            ),
          ),
          child: Row(
            children: <Widget>[
              Expanded(
                child: DropdownButtonHideUnderline(
                  child: DropdownButton(
                    icon: Icon(Icons.keyboard_arrow_down),
                    iconSize: 30,
                    iconEnabledColor: Theme.of(context).accentColor,
                    items: serviceTimeCostUnitListDropdownOptions(),
                    value: _selectedServiceTimeCostUnit,
                    onChanged: (value) {
                      setState(() {
                        _selectedServiceTimeCostUnit = value;
                      });
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildServiceDetailsTextField() {
    return Padding(
      padding: const EdgeInsets.only(right: 50.0, left: 10.0),
      child: TextFormField(
        onSaved: (String value) {
          _formData['service_details'] = value;
        },
        minLines: 3,
        maxLines: 6,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          icon: Icon(Icons.info, color: Theme.of(context).primaryColorLight),
          labelText: 'Detalles del servicio',
          labelStyle: TextStyle(color: Theme.of(context).accentColor),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide(color: Theme.of(context).accentColor),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide(color: Theme.of(context).focusColor),
          ),
          filled: true,
          fillColor: Colors.white,
        ),
        controller: _serviceDetailsTextController,
        validator: (String value) {
          if (value.isEmpty) {
            return 'Por favor ingresa los detalles de tu servicio';
          }
          return null;
        },
      ),
    );
  }

  Widget _buildServiceCommentsTextField() {
    return Padding(
      padding: const EdgeInsets.only(right: 50.0, left: 10.0),
      child: TextFormField(
        onSaved: (String value) {
          _formData['service_comments'] = value;
        },
        minLines: 3,
        maxLines: 6,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          icon: Icon(Icons.comment, color: Theme.of(context).primaryColorLight),
          labelText: 'Comentarios adicionales',
          labelStyle: TextStyle(color: Theme.of(context).accentColor),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide(color: Theme.of(context).accentColor),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide(color: Theme.of(context).focusColor),
          ),
          filled: true,
          fillColor: Colors.white,
        ),
        controller: _serviceCommentsTextController,
        validator: (String value) {
          if (value.isEmpty) {
            return 'Por favor ingresa el título del servicio';
          }
          return null;
        },
      ),
    );
  }

  Widget _buildAddServiceButton() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50.0),
      child: ButtonTheme(
        minWidth: 500.0,
        height: 50.0,
        buttonColor: Theme.of(context).buttonColor,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(5.0),
        ),
        child: RaisedButton(
          onPressed: () {
            _submitForm();
          },
          child: Text('Agregar servicio'),
          textColor: Colors.white,
        ),
      ),
    );
  }

  void _submitForm() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    Navigator.pushNamed(context, 'profile');
  }

  Widget _addServicePage() {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            'Agrega un servicio',
          ),
        ),
        actions: <Widget>[
          PopupMenuButton(
            itemBuilder: (BuildContext context) => <PopupMenuEntry>[
              // const PopupMenuItem(
              //   value: 'configuration',
              //   child: Text('Configuración'),
              // ),
              const PopupMenuItem(
                value: 'logout',
                child: Text('Cerrar sesión'),
              ),
            ],
            onSelected: (value) {
              if (value == 'logout') {
                Navigator.pushReplacementNamed(context, 'auth');
              }
            },
          ),
        ],
      ),
      body: Container(
        // padding: EdgeInsets.all(20.0),
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(height: 30.0),
                Text(
                  'Selecciona una categoría *',
                  style: TextStyle(color: Colors.blueGrey),
                ),
                SizedBox(height: 20.0),
                _buildServiceCategoryDropdown(),
                SizedBox(height: 20.0),
                Text(
                  'Selecciona el tipo de servicio *',
                  style: TextStyle(color: Colors.blueGrey),
                ),
                SizedBox(height: 20.0),
                _buildServiceTypeDropdown(),
                SizedBox(height: 20.0),
                Divider(),
                SizedBox(height: 30.0),
                _buildServiceTittleTextField(),
                SizedBox(height: 20.0),
                _buildServiceCostTextField(),
                SizedBox(height: 20.0),
                Text(
                  'Selecciona la unidad de cobro',
                  style: TextStyle(color: Colors.blueGrey),
                ),
                SizedBox(height: 20.0),
                _buildServiceTimeCostUnitDropdown(),
                SizedBox(height: 20.0),
                Divider(),
                SizedBox(height: 30.0),
                _buildServiceTagTextField(),
                SizedBox(height: 30.0),
                _buildServiceDetailsTextField(),
                SizedBox(height: 30.0),
                _buildServiceCommentsTextField(),
                SizedBox(height: 30.0),
                _buildAddServiceButton(),
                SizedBox(height: 30.0),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: new BottomNavigationBar(
        // backgroundColor: Theme.of(context).primaryColor,
        // backgroundColor: Colors.white70,
        backgroundColor: Colors.white,
        currentIndex: currentIndex,
        onTap: (index) {
          setState(() {
            currentIndex = index;
          });
        },
        type: BottomNavigationBarType.fixed,
        // fixedColor: Theme.of(context).primaryColor,
        items: [
          new BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
              color: Colors.black,
            ),
            title: new Text(
              'Perfíl',
              style: TextStyle(color: Colors.grey),
            ),
          ),
          new BottomNavigationBarItem(
            icon: Icon(
              Icons.add_box,
              color: Colors.black,
            ),
            title: new Text('Agregar servicio'),
          ),
          new BottomNavigationBarItem(
            icon: Icon(
              Icons.category,
              color: Colors.black,
            ),
            title: new Text('Categorías'),
          ),
        ],
      ),
    );
  }

  Widget _bottomNavigationSelect(int currentPage) {
    switch (currentPage) {
      case 0:
        return ProfilePage();

      case 1:
        return _addServicePage();
      case 2:
        return CategoriesPage();
      default:
        return ProfilePage();
    }
  }

  @override
  Widget build(BuildContext context) {
    return _bottomNavigationSelect(currentIndex);
  }
}
