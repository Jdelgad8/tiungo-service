// import 'dart:developer';

import 'package:flutter/material.dart';
// import 'package:graphql_flutter/graphql_flutter.dart';

class SelectCountryPage extends StatefulWidget {
  @override
  _SelectCountryPageState createState() => _SelectCountryPageState();
}

class _SelectCountryPageState extends State<SelectCountryPage> {
  List<String> _countriesList = [
    'Colombia',
    'Venezuela',
    'Perú',
    'Brasil',
  ];
  List<String> _citiesList = [
    'Villavicencio',
    'Bogotá',
    'Medellín',
  ];

  String _selectedCity;
  // String _selectedCountry;

  List<DropdownMenuItem<String>> citiesDropdownOptions() {
    List<DropdownMenuItem<String>> citiesList = new List();
    _citiesList.forEach((cities) {
      citiesList.add(DropdownMenuItem(
        child: Text(cities),
        value: cities,
      ));
    });
    return citiesList;
  }

  List<DropdownMenuItem<String>> countriesDropdownOptions() {
    List<DropdownMenuItem<String>> countriesList = new List();
    _countriesList.forEach((country) {
      countriesList.add(DropdownMenuItem(
        child: Text(country),
        value: country,
      ));
    });
    return countriesList;
  }

  // Widget _buildCountryDropdown() {
  //   String userLoginsQuery = """
  //     {
  //       userLogins{
  //         id
  //         user_login_email
  //         user_login_phone_number
  //       }
  //     }
  //   """;

  //   List<DropdownMenuItem<String>> citiesList = new List();

  //   return Query(
  //     options: QueryOptions(
  //       document: userLoginsQuery,
  //       variables: {'nCities': 2},
  //       pollInterval: 2,
  //     ),
  //     builder: (QueryResult result,
  //         {VoidCallback refetch, FetchMore fetchMore}) {
  //       log(result.toString());
  //       if (result.errors != null) {
  //         return Text(result.errors.toString());
  //       }

  //       if (result.loading) {
  //         return Center(
  //           child: Text('loading...'),
  //         );
  //       }

  //       List cities = result.data['city_name'];
  //       cities.forEach((city) {
  //         citiesList.add(DropdownMenuItem(
  //           child: Text(city),
  //           value: city,
  //         ));
  //         return city;
  //       });
  //       return ButtonTheme(
  //         minWidth: 500.0,
  //         alignedDropdown: true,
  //         child: Container(
  //           width: 500.0,
  //           padding: EdgeInsets.symmetric(horizontal: 10.0),
  //           decoration: BoxDecoration(
  //             borderRadius: BorderRadius.circular(5.0),
  //             border: Border.all(
  //               color: Theme.of(context).primaryColorLight,
  //               style: BorderStyle.solid,
  //               width: 0.80,
  //             ),
  //           ),
  //           child: DropdownButtonHideUnderline(
  //             child: DropdownButton(
  //               icon: Icon(Icons.keyboard_arrow_down),
  //               iconSize: 30,
  //               iconEnabledColor: Theme.of(context).primaryColorLight,
  //               elevation: 16,
  //               items: citiesList,
  //               value: _selectedCountry,
  //               onChanged: (value) {
  //                 setState(() {
  //                   _selectedCountry = value;
  //                 });
  //               },
  //             ),
  //           ),
  //         ),
  //       );
  //     },
  //   );
  // }

  Widget _buildCountryDropdown() {
    return ButtonTheme(
      minWidth: 500.0,
      alignedDropdown: true,
      child: Container(
        width: 500.0,
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(
            color: Theme.of(context).accentColor,
            style: BorderStyle.solid,
            width: 0.80,
          ),
        ),
        child: DropdownButtonHideUnderline(
          child: DropdownButton(
            icon: Icon(Icons.keyboard_arrow_down),
            iconSize: 30,
            iconEnabledColor: Theme.of(context).accentColor,
            items: countriesDropdownOptions(),
            value: _selectedCity,
            onChanged: (value) {
              setState(() {
                _selectedCity = value;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget _buildCityDropdown() {
    return ButtonTheme(
      minWidth: 500.0,
      alignedDropdown: true,
      child: Container(
        width: 500.0,
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(
            color: Theme.of(context).accentColor,
            style: BorderStyle.solid,
            width: 0.80,
          ),
        ),
        child: DropdownButtonHideUnderline(
          child: DropdownButton(
            icon: Icon(Icons.keyboard_arrow_down),
            iconSize: 30,
            iconEnabledColor: Theme.of(context).accentColor,
            items: citiesDropdownOptions(),
            value: _selectedCity,
            onChanged: (value) {
              setState(() {
                _selectedCity = value;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget _buildNextButton() {
    return ButtonTheme(
      minWidth: 500.0,
      height: 50.0,
      buttonColor: Theme.of(context).buttonColor,
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(5.0),
      ),
      child: RaisedButton(
        onPressed: () {
          Navigator.pushNamed(context, 'auth');
        },
        child: Text(
          'Siguiente',
          style: TextStyle(fontSize: 20.0),
        ),
        textColor: Colors.white,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Center(
          child: Text(
            'Selecciona tu ubicación',
            style: TextStyle(fontSize: 20.0),
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 50.0),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 80.0,
            ),
            Center(
              child: new Image.asset(
                'assets/full_logo.png',
                width: 151.5,
                height: 71.0,
              ),
            ),
            SizedBox(height: 40.0),
            Text(
              'Selecciona tu país',
              style: TextStyle(color: Colors.blueGrey, fontSize: 20.0),
            ),
            SizedBox(
              height: 15.0,
            ),
            _buildCountryDropdown(),
            SizedBox(height: 15.0),
            Text(
              'Selecciona tu ciudad',
              style: TextStyle(color: Colors.blueGrey, fontSize: 20.0),
            ),
            SizedBox(
              height: 15.0,
            ),
            _buildCityDropdown(),
            SizedBox(
              height: 40.0,
            ),
            _buildNextButton(),
          ],
        ),
      ),
    );
  }
}
