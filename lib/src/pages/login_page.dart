import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Map<String, String> _formData = {
    'email': null,
    'password': null,
  };
  final TextEditingController _emailTextController = TextEditingController();
  final TextEditingController _passwordTextController = TextEditingController();

  Widget _buildEmailTextField() {
    return TextFormField(
      onSaved: (String value) {
        _formData['email'] = value;
      },
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.email, color: Theme.of(context).primaryColorLight),
        labelText: 'Correo electrónico',
        labelStyle: TextStyle(color: Colors.black),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Colors.black),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Colors.black),
        ),
        filled: true,
        fillColor: Colors.white,
      ),
      controller: _emailTextController,
      validator: (String value) {
        if (value.isEmpty ||
            !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                .hasMatch(value)) {
          return 'Por favor ingresa un correo válido';
        }
        return null;
      },
    );
  }

  Widget _buildPasswordTextField() {
    return TextFormField(
      onSaved: (String value) {
        _formData['password'] = value;
      },
      obscureText: true,
      decoration: InputDecoration(
        focusColor: Theme.of(context).primaryColorLight,
        icon: Icon(Icons.lock_outline,
            color: Theme.of(context).primaryColorLight),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Colors.black),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).primaryColorLight),
        ),
        labelText: 'Contraseña',
        labelStyle: TextStyle(color: Colors.black),
        filled: true,
        fillColor: Colors.white,
      ),
      controller: _passwordTextController,
      validator: (String value) {
        if (value.isEmpty || value.length < 6) {
          return 'Por favor ingrese una contraseña al menos de 6 caracteres';
        }
        return null;
      },
    );
  }

  Widget _buildLoginButton() {
    return ButtonTheme(
      minWidth: 500.0,
      height: 50.0,
      buttonColor: Theme.of(context).buttonColor,
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(5.0),
      ),
      child: RaisedButton(
        onPressed: () {
          _submitForm();
        },
        child: Text(
          'Ingresar',
          style: TextStyle(fontSize: 20.0),
        ),
        textColor: Colors.white,
      ),
    );
  }

  void _submitForm() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    Navigator.pushNamed(context, 'profile');
  }

  @override
  Widget build(BuildContext context) {
    final double deviceWith = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWith > 550.0 ? 500.0 : deviceWith * 0.95;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Padding(
          padding: const EdgeInsets.only(right: 70.0),
          child: Text(
            'Inicia sesión',
            style: TextStyle(fontSize: 20.0),
          ),
        ),
      ),
      body: Container(
        // padding: EdgeInsets.all(40.0),
        child: Center(
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Container(
                width: targetWidth,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    // Row(
                    //   children: <Widget>[
                    //     Icon(
                    //       Icons.email,
                    //       color: Theme.of(context).primaryColorLight,
                    //     ),
                    _buildEmailTextField(),
                    //   ],
                    // ),
                    SizedBox(height: 15.0),
                    // Row(

                    //   children: <Widget>[
                    //     Icon(
                    //       Icons.lock,
                    //       color: Theme.of(context).primaryColorLight,
                    //     ),
                    _buildPasswordTextField(),
                    //   ],
                    // ),
                    SizedBox(height: 30.0),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 50.0),
                      child: _buildLoginButton(),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
