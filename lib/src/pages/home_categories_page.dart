import 'package:flutter/material.dart';

class HomeCategoriesPage extends StatefulWidget {
  @override
  _HomeCategoriesPageState createState() => _HomeCategoriesPageState();
}

class _HomeCategoriesPageState extends State<HomeCategoriesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Hogar'),
      ),
      body: ListView(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.pushNamed(context, 'plomeria_users');
                },
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(8.0),
                      child: Image.asset(
                        'assets/plomeria.jpg',
                        height: 100.0,
                        width: 100.0,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 40.0),
                      child: Center(
                        child: Text(
                          'Plomería',
                          style: TextStyle(
                              fontSize: 30.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Divider(),
              FlatButton(
                onPressed: () {},
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(8.0),
                      child: Image.asset(
                        'assets/carpinteria.jpg',
                        height: 100.0,
                        width: 100.0,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 40.0),
                      child: Center(
                        child: Text(
                          'Carpintería',
                          style: TextStyle(
                              fontSize: 30.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Divider(),
              FlatButton(
                onPressed: () {},
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(8.0),
                      child: Image.asset(
                        'assets/aseo.jpg',
                        height: 100.0,
                        width: 100.0,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 40.0),
                      child: Center(
                        child: Text(
                          'Aseo',
                          style: TextStyle(
                              fontSize: 30.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Divider(),
              FlatButton(
                onPressed: () {},
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(8.0),
                      child: Image.asset(
                        'assets/cerrajero.png',
                        height: 100.0,
                        width: 100.0,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 40.0),
                      child: Center(
                        child: Text(
                          'Cerrajería',
                          style: TextStyle(
                              fontSize: 30.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Divider(),
              FlatButton(
                onPressed: () {},
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(8.0),
                      child: Image.asset(
                        'assets/energia.jpg',
                        height: 100.0,
                        width: 100.0,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 40.0),
                      child: Center(
                        child: Text(
                          'Energía',
                          style: TextStyle(
                              fontSize: 30.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
