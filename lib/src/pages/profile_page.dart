import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:tiungo_service_client/src/pages/add_service_page.dart';
import 'package:tiungo_service_client/src/pages/categories_page.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

enum menuOptions { configuracion, salir }

class _ProfilePageState extends State<ProfilePage> {
  int currentIndex = 0;

  Widget _userProfilePic() {
    return CircleAvatar(
      radius: 50.0,
      backgroundImage: NetworkImage(
        'https://scontent.fbog10-1.fna.fbcdn.net/v/t1.0-9/36803760_10156299460118280_6286547981957070848_n.jpg?_nc_cat=104&_nc_oc=AQl4GB7Vpxzij2s5foB8IIhkWSC5ZVmlYVEv-LQpJEikwgj4TJ0cZAGYq5SsyRZq8Fc&_nc_ht=scontent.fbog10-1.fna&oh=d31bc9bfddcd487f8aaf177094642986&oe=5E419C6E',
      ),
    );
  }

  Widget _userRatingBar() {
    return Container(
      height: 50.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50.0),
        border: Border.all(
          color: Theme.of(context).buttonColor,
          style: BorderStyle.solid,
          width: 3.0,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Row(
          children: <Widget>[
            Text(
              '4.5',
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              width: 10.0,
            ),
            Icon(
              Icons.star,
              color: Colors.yellow,
            ),
            Icon(
              Icons.star,
              color: Colors.yellow,
            ),
            Icon(
              Icons.star,
              color: Colors.yellow,
            ),
            Icon(
              Icons.star,
              color: Colors.yellow,
            ),
            Icon(
              Icons.star_half,
              color: Colors.yellow,
            ),
            SizedBox(
              width: 15.0,
            ),
            Text(
              '3 servicios',
              style: TextStyle(fontSize: 10.0, color: Colors.grey),
            )
          ],
        ),
      ),
    );
  }

  Widget _userInfoDisplay() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
      width: 2000.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _userProfilePic(),
          SizedBox(
            height: 10.0,
          ),
          Text(
            'Jordy Delgado',
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(
            height: 5.0,
          ),
          Text(
            'Ingeniero de Sistemas',
            style: TextStyle(fontSize: 15.0, color: Colors.grey),
          ),
          SizedBox(
            height: 20.0,
          ),
          _userRatingBar(),
        ],
      ),
    );
  }

  Widget _userHistory() {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 50.0),
        child: Column(
          children: <Widget>[
            _userInfoDisplay(),
            SizedBox(
              height: 10.0,
            ),
            _userProfileDisplay(),
          ],
        ),
      ),
    );
  }

  Widget _clientHistory() {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 50.0),
        child: Column(
          children: <Widget>[
            _userInfoDisplay(),
            SizedBox(
              height: 10.0,
            ),
            _clientProfileDisplay(),
          ],
        ),
      ),
    );
  }

  Widget _userProfileDisplay() {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: Theme.of(context).buttonColor,
          style: BorderStyle.solid,
          width: 2.0,
        ),
      ),
      child: Column(
        children: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
                Image.asset(
                  'assets/mobile_service.jpg',
                  width: 80.0,
                  height: 80.0,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Desarrollo aplicación',
                        style: TextStyle(
                            fontSize: 15.0, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Tomado por: Valerin Correa',
                        style: TextStyle(fontSize: 10.0, color: Colors.grey),
                      ),
                      Text(
                        'Ubicación: Villavicencio - Meta',
                        style: TextStyle(fontSize: 10.0, color: Colors.grey),
                      ),
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star_half,
                            color: Colors.yellow,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Theme.of(context).buttonColor,
          ),
          Container(
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.asset(
                    'assets/web_service.png',
                    width: 72.0,
                    height: 72.0,
                  ),
                ),
                Container(
                  // width: 2000.0,
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Text(
                          'Desarrollo página web',
                          style: TextStyle(
                              fontSize: 15.0, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          'Tomado por: Carlos Vasquez',
                          style: TextStyle(fontSize: 10.0, color: Colors.grey),
                        ),
                        Text(
                          'Ubicación: Medellín - Antioquia',
                          style: TextStyle(fontSize: 10.0, color: Colors.grey),
                        ),
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.star,
                              color: Colors.yellow,
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.yellow,
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.yellow,
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.yellow,
                            ),
                            Icon(
                              Icons.star_border,
                              color: Colors.yellow,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Theme.of(context).buttonColor,
          ),
          Container(
            child: Row(
              children: <Widget>[
                Image.asset(
                  'assets/sport_service.jpg',
                  width: 90.0,
                  height: 80.0,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Asesoría deportiva',
                        style: TextStyle(
                            fontSize: 15.0, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Tomado por: Lionel Messi',
                        style: TextStyle(fontSize: 10.0, color: Colors.grey),
                      ),
                      Text(
                        'Ubicación: Barcelona - España',
                        style: TextStyle(fontSize: 10.0, color: Colors.grey),
                      ),
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _clientProfileDisplay() {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: Theme.of(context).buttonColor,
          style: BorderStyle.solid,
          width: 2.0,
        ),
      ),
      child: Column(
        children: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
                Image.asset(
                  'assets/contador_service.png',
                  width: 80.0,
                  height: 80.0,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Declaración de renta',
                        style: TextStyle(
                            fontSize: 15.0, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Atendido por: Javier Monsalve',
                        style: TextStyle(fontSize: 10.0, color: Colors.grey),
                      ),
                      Text(
                        'Ubicación: Villavicencio - Meta',
                        style: TextStyle(fontSize: 10.0, color: Colors.grey),
                      ),
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star_border,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star_border,
                            color: Colors.yellow,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Theme.of(context).buttonColor,
          ),
          Container(
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.asset(
                    'assets/abogado_service.png',
                    width: 72.0,
                    height: 72.0,
                  ),
                ),
                Container(
                  // width: 2000.0,
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Text(
                          'Proceso jurídico',
                          style: TextStyle(
                              fontSize: 15.0, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          'Atentido por: Silva Ponte',
                          style: TextStyle(fontSize: 10.0, color: Colors.grey),
                        ),
                        Text(
                          'Ubicación: Restrepo - Meta',
                          style: TextStyle(fontSize: 10.0, color: Colors.grey),
                        ),
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.star,
                              color: Colors.yellow,
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.yellow,
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.yellow,
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.yellow,
                            ),
                            Icon(
                              Icons.star_border,
                              color: Colors.yellow,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Theme.of(context).buttonColor,
          ),
          Container(
            child: Row(
              children: <Widget>[
                Image.asset(
                  'assets/carpintero_service.png',
                  width: 90.0,
                  height: 80.0,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Mesa de noche',
                        style: TextStyle(
                            fontSize: 15.0, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Atendido por: Lina María',
                        style: TextStyle(fontSize: 10.0, color: Colors.grey),
                      ),
                      Text(
                        'Ubicación: Apartadó - Antioquia',
                        style: TextStyle(fontSize: 10.0, color: Colors.grey),
                      ),
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _profilePage(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Theme.of(context).primaryColor,
          title: Text(
            'Perfíl',
            style: TextStyle(fontSize: 20.0),
          ),
          actions: <Widget>[
            PopupMenuButton(
              itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                // const PopupMenuItem(
                //   value: 'configuration',
                //   child: Text('Configuración'),
                // ),
                const PopupMenuItem(
                  value: 'logout',
                  child: Text('Cerrar sesión'),
                ),
              ],
              onSelected: (value) {
                if (value == 'logout') {
                  Navigator.pushReplacementNamed(context, 'auth');
                }
                log(value.toString() + ' button pressed');
              },
            ),
          ],
          bottom: TabBar(
            tabs: <Widget>[
              Tab(
                text: 'Usuario',
              ),
              Tab(
                text: 'Cliente',
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            _userHistory(),
            _clientHistory(),
          ],
        ),
        bottomNavigationBar: new BottomNavigationBar(
          // backgroundColor: Theme.of(context).primaryColor,
          // backgroundColor: Colors.white70,
          backgroundColor: Colors.white,
          currentIndex: currentIndex,
          onTap: (index) {
            setState(() {
              currentIndex = index;
            });
          },
          type: BottomNavigationBarType.fixed,
          // fixedColor: Theme.of(context).primaryColor,
          items: [
            new BottomNavigationBarItem(
              icon: Icon(
                Icons.person,
                color: Colors.black,
              ),
              title: new Text(
                'Perfíl',
                style: TextStyle(color: Colors.grey),
              ),
            ),
            new BottomNavigationBarItem(
              icon: Icon(
                Icons.add_box,
                color: Colors.black,
              ),
              title: new Text('Agregar servicio'),
            ),
            new BottomNavigationBarItem(
              icon: Icon(
                Icons.category,
                color: Colors.black,
              ),
              title: new Text('Categorías'),
            ),
          ],
        ),
      ),
    );
  }

  Widget _bottomNavigationSelect(BuildContext context, int currentPage) {
    switch (currentPage) {
      case 0:
        return _profilePage(context);

      case 1:
        return AddServicePage();
      case 2:
        return CategoriesPage();
      default:
        return _profilePage(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return _bottomNavigationSelect(context, currentIndex);
  }
}
