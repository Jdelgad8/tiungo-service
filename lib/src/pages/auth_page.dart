import 'package:flutter/material.dart';

class AuthPage extends StatefulWidget {
  @override
  _AuthPageState createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  Widget _buildSignupButton() {
    return ButtonTheme(
      minWidth: 500.0,
      height: 50.0,
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(5.0),
      ),
      buttonColor: Theme.of(context).buttonColor,
      child: RaisedButton(
        onPressed: () {
          Navigator.pushNamed(context, 'signup');
        },
        child: Text(
          'Registrate',
          style: TextStyle(fontSize: 20.0),
        ),
        textColor: Colors.white,
      ),
    );
  }

  Widget _buildLoginButton() {
    return ButtonTheme(
        minWidth: 500.0,
        height: 50.0,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(5.0),
        ),
        buttonColor: Theme.of(context).buttonColor,
        child: RaisedButton(
          onPressed: () {
            Navigator.pushNamed(context, 'login');
          },
          child: Text(
            'Inicia sesión',
            style: TextStyle(fontSize: 20.0),
          ),
          textColor: Colors.white,
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text('Ingresa o registrate'),
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 50.0),
        child: Column(
          children: <Widget>[
            SizedBox(height: 80.0),
            Center(
              child: new Image.asset(
                'assets/full_logo.png',
                width: 151.5,
                height: 71.0,
              ),
            ),
            SizedBox(height: 100.0),
            _buildSignupButton(),
            SizedBox(height: 30.0),
            _buildLoginButton(),
            SizedBox(height: 20.0),
            Text(
              'o ingresa con',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.blueGrey, fontSize: 15.0),
            ),
            SizedBox(height: 20.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Image.asset(
                  'assets/facebook_logo.png',
                  width: 40.0,
                  height: 40.0,
                ),
                SizedBox(
                  width: 50.0,
                ),
                new Image.asset(
                  'assets/google_logo.png',
                  width: 40.0,
                  height: 40.0,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
