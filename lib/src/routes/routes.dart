import 'package:flutter/material.dart';
import 'package:tiungo_service_client/src/pages/add_service_page.dart';
import 'package:tiungo_service_client/src/pages/auth_page.dart';
import 'package:tiungo_service_client/src/pages/categories_page.dart';
import 'package:tiungo_service_client/src/pages/home_categories_page.dart';
import 'package:tiungo_service_client/src/pages/login_page.dart';
import 'package:tiungo_service_client/src/pages/plomeria_users_page.dart';
import 'package:tiungo_service_client/src/pages/profile_page.dart';
import 'package:tiungo_service_client/src/pages/select_country_page.dart';
import 'package:tiungo_service_client/src/pages/signup_page.dart';
import 'package:tiungo_service_client/src/pages/signup_personal_data_page.dart';

Map<String, WidgetBuilder> getAppRoutes() {
  return <String, WidgetBuilder>{
    'select_country': (BuildContext context) => SelectCountryPage(),
    'auth': (BuildContext context) => AuthPage(),
    'login': (BuildContext context) => LoginPage(),
    'signup': (BuildContext context) => SignupPage(),
    'signup_personal_data': (BuildContext context) => SignupPersonalDataPage(),
    'add_service': (BuildContext context) => AddServicePage(),
    'profile': (BuildContext context) => ProfilePage(),
    'categories': (BuildContext context) => CategoriesPage(),
    'home_categories': (BuildContext context) => HomeCategoriesPage(),
    'plomeria_users': (BuildContext context) => PlomeriaUsersPage(),
  };
}
