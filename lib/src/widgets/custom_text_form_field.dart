import 'package:flutter/material.dart';

class CustomTextFormField extends StatelessWidget {
  // final TextEditingController textFormFieldController;
  // final String labelText;
  // CustomTextFormField({this.textFormFieldController});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onSaved: (String value) {
        // _formData['cellphone'] = value;
      },
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(
        icon: Icon(Icons.phone_android,
            color: Theme.of(context).primaryColorLight),
        labelText: 'Celular',
        labelStyle: TextStyle(color: Colors.black),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Colors.black),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Colors.black),
        ),
        filled: true,
        fillColor: Colors.white,
      ),
      // controller: textFormFieldController,
      validator: (String value) {
        if (value.isEmpty || value.length < 10) {
          return 'Por favor ingresa un número de celular válido';
        }
        return null;
      },
    );
  }
}
