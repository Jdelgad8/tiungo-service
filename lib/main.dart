import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:tiungo_service_client/src/routes/routes.dart';

void main() {
  // final HttpLink httpLink = HttpLink(uri: 'http://localhost:3116/graphql?');
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(
      client: _client(),
      child: MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en', 'US'),
          const Locale('es', 'ES'),
        ],
        theme: ThemeData(
          brightness: Brightness.light,
          // Color naranja principal (Naranja)
          primaryColor: Color.fromRGBO(230, 93, 37, 1.0),
          // primaryColor: Colors.teal,
          // primaryColor: Colors.indigo,
          // primaryColor: Colors.pink,
          // Color principal para botones (Azul claro)
          buttonColor: Color.fromRGBO(70, 195, 230, 1.0),
          // Color principal de confirmación de accion (Verde claro)
          // buttonColor: Color.fromRGBO(230, 93, 37, 1.0),
          // Color de fondo de la app
          backgroundColor: Colors.white,
          // Color principal para íconos (Amarillo)
          // primaryColorLight: Color.fromRGBO(250, 175, 24, 1.0),
          primaryColorLight: Color.fromRGBO(250, 175, 24, 1.0),
          accentColor: Colors.black,
          focusColor: Colors.grey,
          fontFamily: '',
        ),
        title: 'Tiungo service',
        initialRoute: 'select_country',
        routes: getAppRoutes(),
        // onGenerateRoute: (RouteSettings settings) {
        //   return MaterialPageRoute(
        //     builder: (BuildContext context) => SelectCountryPage(),
        //   );
        // },
      ),
    );
  }

  ValueNotifier<GraphQLClient> _client() {
    final HttpLink _httpLink = HttpLink(uri: 'http://localhost:3116/graphql');

    return ValueNotifier(GraphQLClient(
      cache: OptimisticCache(
        dataIdFromObject: typenameDataIdFromObject,
      ),
      link: _httpLink,
    ));
  }
}
